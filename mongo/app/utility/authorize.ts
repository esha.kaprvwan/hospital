import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";
import { IExcludePath } from "../routes/routes.types";
import { ResponseHandler } from "./response";

export const authorize = (excludedPaths: IExcludePath[]) => {
    return (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            if (excludedPaths.find(e =>
                e.method === req.method &&
                e.path === req.url
            )) {
                return next();
            }

            const { SECRET_KEY } = process.env;
            let token = req.headers.authorization || '';
            const payload = verify(req.headers.authorization || '', SECRET_KEY || '');
            res.locals.user = payload;
            next();
        } catch (e) {
            next({ statusCode: 401, message: 'UNAUTHORIZED' });
        }
    }
}

export const permit = (rolesArray: string[]) => {


    return (req: Request, res: Response, next: NextFunction) => {
        try {
            const roleArrFromDB = res.locals.user.Role;
            // console.log('inside permit');
            // console.log(roleArrFromDB);
            console.log("permit")
            if (!rolesArray.some(role => roleArrFromDB.includes(role))) {
                return res.status(401).send(new ResponseHandler(null, { message: 'Unauthorized' }));
            }
            next();
        } catch (error) {
            console.log(error);

        }
    }
}