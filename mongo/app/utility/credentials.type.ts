export interface ICredentials {
    emp_id: string;
    password: string;
}