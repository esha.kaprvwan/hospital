import { IExcludePath, Route } from "./routes.types";
import UserRouter from "../modules/user/user.routes";
import DoctorRouter from "../modules/doctor/doctor.routes";
import NurseRouter from "../modules/nurse/nurse.routes"
export const routes = [
    new Route('/user', UserRouter),
    new Route('/doctor',DoctorRouter),
    new Route('/nurse',NurseRouter),
];
export const excludedPaths: IExcludePath[] = [
    { method: 'POST', path: '/user/login' },
    { method: 'POST', path: '/user' }
];