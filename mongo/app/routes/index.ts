import { Application, json, NextFunction, Request, Response } from "express"
import helmet from "helmet";
import { ResponseHandler } from "../utility/response";
import { routes , excludedPaths} from "./routes.data";
import { authorize } from "../utility/authorize";
//import { IExcludePath } from "./routes.types";
export const registerRoutes = (app: Application) => {
    app.use(json());
    app.use(helmet());
    app.use(authorize(excludedPaths));
    for (const route of routes) {
        app.use(route.path, route.router);
    }

    app.use((error: any, req: Request, res: Response, next: NextFunction) => {
        res.status(error.statusCode ?? 500).send(new ResponseHandler(null, error));
    });
}