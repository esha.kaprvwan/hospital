import { connect } from "mongoose"

export const connectToMongo = async () => {
    // code to connect to db
    // throw error if connection fails
    try {
        const { MONGO_CONNECTION } = process.env;
        await connect(MONGO_CONNECTION || '');
        console.log("Connected to the database")
        return true;



    } catch (e) {
        throw { message: "Couldnot connect to the database" }

    }
}