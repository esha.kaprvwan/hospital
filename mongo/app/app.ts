import express from "express";
import { connectToMongo } from "./connections/connect.mongo";
import { registerRoutes } from "./routes";

export const startServer = async () => {
    try {
        const app = express();

        await connectToMongo();
        registerRoutes(app);

        const { PORT } = process.env;
        app.listen(
            PORT,
            () => console.log(`SERVER STARTED ON PORT ${PORT}`)
        );
    } catch (error) {
        console.log('Failed to start the server.');
        process.exit(1); // 0 -> Everything was fine and now program exits
    }
}
