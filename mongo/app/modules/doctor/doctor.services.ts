import doctorRepo from "./doctor.repo";
import { IDoctor } from "./doctor.types";

const createDoctor = (doctor: IDoctor) => doctorRepo.createDoctor(doctor);
const findDoctor = (name: string) => doctorRepo.findDoctor(name);
const updateNurse = (emp_id: any, nurse_id: any) => doctorRepo.updateNurse(emp_id, nurse_id);
const updateDoctor = (doctor: IDoctor) => doctorRepo.updateDoctor(doctor);
export default {
    createDoctor,
    findDoctor,
    updateNurse,
    updateDoctor,

}