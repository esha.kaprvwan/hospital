import { body } from "express-validator";
import { validate } from "../../utility/validate"

export const doctorValidator = [
    // body("emp_id").isString().withMessage("Id is required"),
    body("name").isString().notEmpty().withMessage("name is required"),
    body("type").isString().notEmpty().withMessage("Type is required"),
    body("assignedNurse").isString().notEmpty().withMessage("Assigned Nurse is required"),
    validate
];