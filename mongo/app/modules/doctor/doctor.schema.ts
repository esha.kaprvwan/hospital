import { Schema, model } from "mongoose";

class DoctorSchema extends Schema {
    constructor() {
        super({
            emp_id: {
                type: Schema.Types.ObjectId,
                ref: "User"
            },
            name: {
                type: String,
                required: true
            },
            type: {
                type: String,
                required: true
            },
            assignedNurse: {
                type: Schema.Types.ObjectId,
                ref: "Nurse",
                default:null
            }



        }, {
            timestamps: true
        })
    }
}

const DoctorModel = model('Doctor', new DoctorSchema());
export default DoctorModel;