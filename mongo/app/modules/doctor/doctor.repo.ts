import DoctorModel from "./doctor.schema";
import { IDoctor } from "./doctor.types";

const createDoctor = (doctor: IDoctor) => DoctorModel.create(doctor);
const findDoctor=(name:string)=>DoctorModel.findOne({name:name});
const updateNurse=(emp_id: any, nurse_id:any  )=>DoctorModel.updateOne({emp_id:emp_id},{ $set: {assignedNurse: nurse_id}})
const updateDoctor=(doctor: IDoctor) =>DoctorModel.updateOne({
    emp_id: doctor.emp_id },doctor);
export default{
    createDoctor,
    findDoctor,
    updateNurse,
    updateDoctor,

}
