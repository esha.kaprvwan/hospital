export interface IDoctor {
    emp_id?: string;
    name: string;
    type: string;
    assignedNurse: string;
}