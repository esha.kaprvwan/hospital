import { NextFunction, Request, Response, Router } from "express";
import doctorServices from "./doctor.services";
import { ResponseHandler } from "../../utility/response";
import { permit } from "../../utility/authorize";
const router = Router();

router.get("/:name", permit(["DOCTOR"]), async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        // console.log(req.body);
        let {name} = req.params;
        const result = await doctorServices.findDoctor(name);
        console.log(result)
        res.send(new ResponseHandler({message:"Fetched doctor details ",data: result}));
    } catch (e) {
        next(e);
    }
});


export default router;