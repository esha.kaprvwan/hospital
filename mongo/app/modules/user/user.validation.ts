import { body } from "express-validator";
import { validate } from "../../utility/validate"

export const RegisterUserValidator = [
    body("Name").isString().notEmpty().withMessage("name is required"),
    body("Dept").isString().notEmpty().withMessage("Department is required"),
    body("Address").isString().notEmpty().withMessage("Address is required"),
    body("Role").isArray().notEmpty().withMessage("Role is required"),
    validate
];
export const LoginValidator = [
    body("emp_id").isString().withMessage("email is required"),
    body("password").isString().notEmpty().withMessage("password is required"),
    validate
];