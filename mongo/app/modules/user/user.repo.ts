import DoctorModel from "../doctor/doctor.schema";
import doctorServices from "../doctor/doctor.services";
import { IDoctor } from "../doctor/doctor.types";
import nurseServices from "../nurse/nurse.services";
import { INurse } from "../nurse/nurse.types";
import UserModel from "./user.schema";
import { IUser } from "./user.types";

const create = (user: IUser) => UserModel.create(user);
const getAll = (role: string) => UserModel.find({ Role: role });
// const getSome=(Role: [])=>UserModel.find({})
const getOne = (emp_id: string, password: string) => UserModel.findOne({ emp_id: emp_id, password: password })
const update = (user: IUser) => UserModel.updateOne({
    emp_id: user.emp_id
}, user)

const deleteOne = (emp_id: string) => UserModel.updateOne({ emp_id: emp_id }, { $set: { isDeleted: false } })

const createDoctor = (doctor: IDoctor) => {

    return DoctorModel.create(doctor);
}
const updateDoctor = (doctor: IDoctor) => doctorServices.updateDoctor(doctor);


// nurse functions 
const createNurse = (nurse: INurse) => nurseServices.createNurse(nurse);
const updateNurse = (nurse: INurse) => nurseServices.updateNurse(nurse);

export default {
    create,
    getOne,
    getAll,
    update,
    deleteOne,
    createDoctor,
    updateDoctor,
    createNurse,
    updateNurse,
}