import { Router, Request, Response, NextFunction } from "express";
import userService from "./user.service";
import { ResponseHandler } from "../../utility/response";
import { LoginValidator, RegisterUserValidator } from "./user.validation";
import { permit } from "../../utility/authorize";
import { doctorValidator } from "../doctor/doctor.validations";
import { Types } from "mongoose"
import { NurseValidator } from "../nurse/nurse.validations";
const router = Router();

router.post("/register", RegisterUserValidator, permit(["ADMIN"]), async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        console.log(req.body);

        const result = await userService.createUser(req.body);
        console.log(result)
        res.send(new ResponseHandler(result));
    } catch (e) {
        next(e);
    }
});

router.post("/login", LoginValidator, async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const { emp_id, password } = req.body;
        const token = await userService.login(emp_id, password);
        // console.log(token)
        res.send(new ResponseHandler({ message: "LOGIN SUCCESSFUL", data: token }));
    } catch (e) {
        console.log(e)
        next(e);
    }
});

router.get("/admin", permit(["ADMIN"]), async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const { role } = req.body;

        const userData = await userService.getAllUser(role)
        res.send(new ResponseHandler({ data: userData }));
    } catch (e) {
        console.log(e);
        next(e);
    }

})
router.put("/admin", permit(["ADMIN"]), async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const userData = req.body;

        const updatedData = await userService.edit(userData)
        res.send(new ResponseHandler({ message: "EDITED SUCCESSFUL", data: updatedData }));
    } catch (e) {
        console.log(e);
        next(e);
    }

})
router.delete("/admin/:id", permit(["ADMIN"]), async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const userID = req.params.id;
        const result = await userService.deleteOne(userID);
        res.send(new ResponseHandler({ message: "DELETION SUCCESSFUL", data: result }));
    } catch (e) {
        console.log(e);
        next(e);
    }

})

//add a doctor 
router.post("/admin/createdoctor", doctorValidator, permit(["ADMIN"]), async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        let { emp_id, name, type, assignedNurse } = req.body;
        emp_id = new Types.ObjectId(emp_id)
        assignedNurse = new Types.ObjectId(assignedNurse)
        let doctor = { emp_id: emp_id, name: name, type: type, assignedNurse: assignedNurse }
        console.log(doctor)
        const result = await userService.createDoctor(doctor);
        console.log(result)

        res.send(new ResponseHandler({ message: "doctor created successfully", data: result }));
    } catch (e) {
        console.log(e)
        next(e);
    }
});


router.put("/admin/editdoctor", doctorValidator, permit(["ADMIN"]), async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        let { emp_id, name, type, assignedNurse } = req.body;
        emp_id = new Types.ObjectId(emp_id)
        assignedNurse = new Types.ObjectId(assignedNurse)
        let doctor = { emp_id: emp_id, name: name, type: type, assignedNurse: assignedNurse }
        const result = await userService.updateDoctor(doctor);
        console.log(result)

        res.send(new ResponseHandler({ message: "doctor edited successfully", data: result }));
    } catch (e) {
        console.log(e)
        next(e);
    }
});

//delete doctor 
//delete nurse 

//nurse 
router.post("/admin/createNurse", NurseValidator, permit(["ADMIN"]), async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        let { emp_id, name, assignedDoctor } = req.body;
        emp_id = new Types.ObjectId(emp_id)
        let nurse = { emp_id: emp_id, name: name, assignedDoctor: assignedDoctor }
        console.log(nurse)
        const result = await userService.createNurse(nurse);
        console.log(result)

        res.send(new ResponseHandler({ message: "Nurse created successfully", data: result }));
    } catch (e) {
        console.log(e)
        next(e);
    }
});
router.put("/admin/editNurse", NurseValidator, permit(["ADMIN"]), async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        let { emp_id, name, assignedDoctor } = req.body;
        emp_id = new Types.ObjectId(emp_id)
        let nurse = { emp_id: emp_id, name: name , assignedDoctor: assignedDoctor }
        console.log(nurse);
        const result = await userService.updateNurse(nurse);
        console.log(result)

        res.send(new ResponseHandler({ message: "Nurse edited successfully", data: result }));
    } catch (e) {
        console.log(e)
        next(e);
    }
});

export default router;