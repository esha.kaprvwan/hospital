import { Schema, model } from "mongoose";

class UserSchema extends Schema {
    constructor() {
        super({
            emp_id: {
                type: String,
                required: false
            },
            Name: {
                type: String,
                required: true
            },
            Dept: {
                type: String,
                required: true
            },
            Address: {
                type: String,
                required: true
            },
            password: {
                type: String,
                required: true
            },
            Role: {          
                type: [String],
                enum: ['ADMIN', 'DOCTOR', 'NURSE'],
                    
                required: true,

            },
            isDeleted:{
                type:Boolean,
                default:false
            }



        }, {
            timestamps: true
        })
    }
}

const UserModel = model('User', new UserSchema());
export default UserModel;