import userRepo from "./user.repo";
import { IUser } from "./user.types";
import { v4 } from 'uuid';
import { sign } from "jsonwebtoken";
import { generate } from "generate-password";
import * as shortid from "shortid"
import { IDoctor } from "../doctor/doctor.types";
import { INurse } from "../nurse/nurse.types";
const createUser = (user: IUser) => {
    try {
        const emp_id = shortid.generate();
        console.log(emp_id);
        const password = generate({
            length: 10,
            numbers: true
        });
        return userRepo.create({ emp_id: emp_id, password: password, ...user });

    } catch (e) {
        throw { message: "Cannot create user" }
    }
}

const login = async (emp_id: string, password: string) => {
    try {
        const userData = await userRepo.getOne(emp_id, password);
        if (!userData) {
            throw { statusCode: 400, message: "INVALID CREDENTIALS" }
        }
        const { SECRET_KEY } = process.env;
        const token = sign(userData.toObject(), SECRET_KEY || '');
        // console.log(token);

        return token;

    } catch (e) {
        console.log(e);
        throw e;
    }
}

const getAllUser = async (roles: string) => {
    try {
        const userData = userRepo.getAll(roles);
        return userData;

    } catch (e) {
        throw e;
    }
}
const edit = (user: IUser) => userRepo.update(user);
const deleteOne = (emp_id: string) => userRepo.deleteOne(emp_id)

//doctor functions 
const createDoctor = (doctor: IDoctor) => {
    try {
        const data = userRepo.createDoctor(doctor);
        console.log(data);
        return data;
    } catch (e) {
        throw (e);
    }

}
const updateDoctor = (doctor: IDoctor) => {
    try {
        const data = userRepo.updateDoctor(doctor);
        return data;
    } catch (e) {
        throw (e);
    }
}
//nurse 
const createNurse=(nurse:INurse)=> userRepo.createNurse(nurse);
const updateNurse=(nurse:INurse)=>userRepo.updateNurse(nurse);
export default {
    createUser,
    login,
    getAllUser,
    edit,
    deleteOne,
    createDoctor,
    updateDoctor,
    createNurse,
    updateNurse,
}