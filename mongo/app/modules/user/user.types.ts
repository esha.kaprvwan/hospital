export interface IUser {
    emp_id?: string;
    password?: string;
    Name: string;
    Dept: string;
    Address: string;
    Role: string[];
}