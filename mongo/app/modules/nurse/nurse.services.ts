import NurseRepo from "./nurse.repo";
import { INurse } from "./nurse.types";

const createNurse = (nurse: INurse) => NurseRepo.createNurse(nurse);
const findNurse = (name: string) => NurseRepo.findNurse(name);
// const updateNurse = (emp_id: any, nurse_id: any) => doctorRepo.updateNurse(emp_id, nurse_id);
 const updateNurse = (nurse: INurse) => NurseRepo.updateNurse(nurse);
export default {
  createNurse,
  findNurse,
  updateNurse,

}