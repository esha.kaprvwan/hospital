import { Schema, model } from "mongoose";

class NurseSchema extends Schema {
    constructor() {
        super({
            emp_id: {
                type: Schema.Types.ObjectId,
                ref: "User"
            },
            name: {
                type: String,
                required: true
            },
            assignedDoctor: {
                type: [Schema.Types.ObjectId],
                ref: "Doctor",
                default:null
            }



        }, {
            timestamps: true
        })
    }
}

const NurseModel = model('Nurse', new NurseSchema());
export default NurseModel;