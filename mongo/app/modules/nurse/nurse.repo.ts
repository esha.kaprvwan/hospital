import NurseModel from "./nurse.schema"
import { INurse } from "./nurse.types"

const createNurse = (nurse: INurse) => NurseModel.create(nurse);
const findNurse=(name:string)=>NurseModel.findOne({name:name});
// const updateNurse=( )=>DoctorModel.updateOne({emp_id:emp_id},{ $set: {assignedNurse: nurse_id}})
const updateNurse=(nurse: INurse) =>NurseModel.updateOne({emp_id: nurse.emp_id },nurse);

export default{
    createNurse,
    findNurse,
    // updateNurse,
    updateNurse,

}