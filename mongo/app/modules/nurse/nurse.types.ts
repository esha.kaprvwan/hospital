export interface INurse {
    emp_id?: string;
    name: string;
    assignedDoctor: string[];
}