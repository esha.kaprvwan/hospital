import { NextFunction, Request, Response, Router } from "express";
import nurseServices from "./nurse.services";
import { ResponseHandler } from "../../utility/response";
import { permit } from "../../utility/authorize";
const router = Router();

router.get("/:name", permit(["NURSE"]), async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        // console.log(req.body);
        let {name} = req.params;
        const result = await nurseServices.findNurse(name);
        console.log(result)
        res.send(new ResponseHandler({message:"Fetched nurse details ",data: result}));
    } catch (e) {
        next(e);
    }
});

// request to assign a new nurse 

export default router;