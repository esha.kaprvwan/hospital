import { body } from "express-validator";
import { validate } from "../../utility/validate"

export const NurseValidator = [
    body("emp_id").isString().withMessage("Id is required"),
    body("name").isString().notEmpty().withMessage("name is required"),
    body("assignedDoctor").isArray().notEmpty().withMessage("Assigned Doctor is required"),
    validate
];